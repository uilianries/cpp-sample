# Yet Another C++ Sample

#### Build
To build this project:

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release
    cmake --build .

#### Test
To test this project:

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release
    cmake --build .
    cmake --build . --target test

#### Install
To install this project:

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release
    cmake --build .
    cmake --build . --target install

#### Licence
[MIT](LICENSE)
