#include "hello/hello.hpp"

#include <cstdlib>
#include <stdexcept>


int main() {
    const hello::Message hello;

    if (hello.get_message().find("Khomp") == std::string::npos) {
        throw std::runtime_error("Ops!");
    }

    return EXIT_SUCCESS;
}
